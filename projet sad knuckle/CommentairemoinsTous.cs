﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class CommentairemoinsTous : Form
    {
        public ArrayList CommMoins = new ArrayList();
        public CommentairemoinsTous()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radio1.Checked == true)
            {
                CommMoins.Add("Bavardage incessant. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio2.Checked == true)
            {
                CommMoins.Add("Dispersion de la classe. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio3.Checked == true)
            {
                CommMoins.Add("Comportement innaproprié de la classe. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radioautre.Checked == true)
            {
                CommMoins.Add(Convert.ToString(richtbautre.Text) + " Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            this.Hide();
        }
    }
}
