﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class CommentaireplusTous : Form
    {
        public ArrayList CommPlus = new ArrayList();
        public CommentaireplusTous()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radio1.Checked == true)
            {
                CommPlus.Add("Classe calme. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio2.Checked == true)
            {
                CommPlus.Add("Bonne écoute des consignes. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio3.Checked == true)
            {
                CommPlus.Add("Respect d'autrui constant. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radioautre.Checked == true)
            {
                CommPlus.Add(Convert.ToString(richtbautre.Text) + " Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            this.Hide();
        }
    }
}
