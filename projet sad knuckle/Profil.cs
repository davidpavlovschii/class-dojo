﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_sad_knuckle
{
    public partial class Profil : Component
    {
        public Profil()
        {
            InitializeComponent();
        }

        public Profil(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
