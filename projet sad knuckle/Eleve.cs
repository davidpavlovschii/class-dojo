﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    class Eleve
    {
        //proprietés
        private string nomEleve;
        private int noteEleve;
       
        //méthodes
        public Eleve()
        {

        }
        public Eleve(string unNom, int uneNote)
        {
            nomEleve = unNom;
            noteEleve = uneNote;
        }

        public void RetirerUnPoint()
        {
            noteEleve--;
        }
        public void AjouterUnPoint()
        {
            noteEleve++;
        }
        public int GetNoteEleve()
        {
            return noteEleve;
        }
        public void SetNoteEleve(int uneNote)
        {
            noteEleve = uneNote;
        }
        public string GetNomEleve()
        {
            return nomEleve;
        }
        public void SetNomEleve(string unNom)
        {
            nomEleve = unNom;
        }
    }
}
