﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace projet_sad_knuckle
{
    [Serializable]
    class Section 
    {
        //les proprietés privés

        private static List<ProfilEleve> listProfEleve = new List<ProfilEleve>(); //création d'une liste typée qui prendra seulement des composants élèves comme elements.
        private string nomSection;
        private static List<Eleve> listEleve = new List<Eleve>();
        private string filePath = @"C:\Users\nader\Desktop\Cours\projet sad knuckle\section.txt"; //permet d'indiquer le chemin d'acces au fichier de lecture / ecriture dans une variable, pour eviter de l'entrer en dure.

        //les méthodes
        public Section() //constructeur de base
        {

        }
    
        public Section(string unNomSection) //constructeur surchargé de la classe section, prend en parametre une chaine de caractere, pour donner son nom tout simplement
        {
            nomSection = unNomSection;
        }
        public Section(List<Eleve> uneListeEleve) //constructeur surchargé de la classe section, prend en parametre une chaine de caractere, pour donner son nom tout simplement
        {
            listEleve = uneListeEleve;
        }
        public static int CompterLesEleves()
        {
            return listEleve.Count();
        }
        public static int GetNoteEleve(int indexEleve)
        {
            return listEleve[indexEleve].GetNoteEleve();
        }
      
        public string GetNomSection() //methode permettant de recuperer le nom d'une section déjà créée
        {
            return nomSection;
        }
        public Eleve GetUnEleve(int unIndex)
        {
            return listEleve[unIndex];
        }
        public void SetNomSection(string unNomSection) //methode permettant de changer le nom d'une section.
        {
            nomSection = unNomSection;
        }
        public static void AjouterProfEleve(ProfilEleve unEleve) //permet d'ajouter un eleve a la liste d'eleves
        {
            listProfEleve.Add(unEleve);
        }
        public static void AjouterEleve(Eleve unEleve) //permet d'ajouter un eleve a la liste d'eleves
        {
            listEleve.Add(unEleve);
        }
        public static List<ProfilEleve> GetListProfEleve() // methode retournant tous les éleves de la listEleves, doit etre stockée dans un arraylist ou une liste typée ProfilEleve
        {
            return listProfEleve;
        }
        public static List<Eleve> GetListEleve() // methode retournant tous les éleves de la listEleves, doit etre stockée dans un arraylist ou une liste typée ProfilEleve
        {
            return listEleve;
        }
        public static void SetListEleve(List<ProfilEleve> uneListeDEleve)
        {
            listProfEleve = uneListeDEleve;
        }
        public static void SetListEleve(List<Eleve> uneListeDEleve)
        {
            listEleve = uneListeDEleve;
        }
        public void PlusUnToutLeMonde() // methode permettant d'attribuer un point a chaque eleve présent dans la liste listEleve
        {
            foreach (ProfilEleve unEleve in listProfEleve) //parcour de la liste listEleve.
            {
                //if (unEleve.lbLaNote
                unEleve.PlusUn(); //methode permettant d'attribuer un point supplementaire a un eleve.
            }
        }
        public void MoinsUnToutLeMonde() //méthode permettant de retirer un point à chaque eleve présent dans la liste listEleve.
        {
            foreach (ProfilEleve unEleve in listProfEleve) //parcour de la liste listEleve.
            {
                unEleve.MoinsUn(); //methode permettant de retirer un point à un éleve.
            }
        }

        public void EnregistrerLaSection() // méthode permettant d'enregistrer les éleves dans un fichier texte.
        {
            string leTexte = nomSection + Environment.NewLine;
            File.WriteAllText(filePath, leTexte);
            foreach (ProfilEleve unEleve in listProfEleve)
            {
                File.AppendAllText(filePath, unEleve.GetNom() + " " + unEleve.GetNote() + "\r");
            }
            MessageBox.Show("La section à bien été créée, les éleves ont bien étés enregistrés");
        }

        public void AfficherLesEleves() // parcour le fichier texte dans lequel les éleves ont étés enregistrés et permet d'afficher chaque éleve present dedans.
        {
            string lireText = File.ReadAllText(filePath);
            MessageBox.Show(lireText);
        }

        public void InterrogationAleatoire() // méthode permettant d'interroger un éleve aleatoirement
        {
            Random interEleveAlea = new Random();
            int eleveInterroge = interEleveAlea.Next(1, listProfEleve.Count());
            ProfilEleve unEleveInteroge = listProfEleve[eleveInterroge];
            MessageBox.Show("L'eleve choisis pour l'interrogation est " + unEleveInteroge.GetNom());
        }

        private void Section_Load(object sender, EventArgs e)
        {

        }
    }
}
