﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    partial class ProfilEleve : UserControl
    {
        Commentaireplus Commentaireplus = new Commentaireplus();
        Commentairemoins Commentairemoins = new Commentairemoins();
        public ProfilEleve()
        {
            InitializeComponent();
        }
        //evenements
        private void ProfEleve_Load(object sender, EventArgs e)
        {

        }
        
        private void btPlus_Click(object sender, EventArgs e) // quand le bouton plus (+) est pressé, attribu un point à l'éleve.
        {
            Commentaireplus.Show();
            PlusUn();
        }

        private void btMoins_Click(object sender, EventArgs e) // quand le bouton moins (-) est pressé, retire un point à l'éleve.
        {
            Commentairemoins.Show();
            MoinsUn();
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            if (tbNom.Text == "")
                MessageBox.Show("Veuillez saisir un nom pour votre eleve");
            else
            {
                lbNomEleve.Visible = true;
                Eleve.SetNomEleve(tbNom.Text);
                lbNomEleve.Text = Eleve.GetNomEleve();
                CacherLesControls();
                Eleve.SetNomEleve(lbNomEleve.Text);
                Eleve.SetNoteEleve(cptNote);
                Section.AjouterEleve(Eleve);
            }
        }
        private void gbEleve_Enter(object sender, EventArgs e)
        {

        }
        //proprietés
        private int cptNote = 0;
        private Eleve Eleve = new Eleve();
        public Eleve GetEleve()
        {
            return Eleve;
        }
        public void SetEleve(Eleve unEleve)
        {
            Eleve = unEleve; 
        }
        //methodes
        public void SetNom(string unNom)
        {
            lbNomEleve.Text = Eleve.GetNomEleve();
        }
        public string GetNom()
        {
            return lbNomEleve.Text;
        }
        public void SetNote(int uneNote)
        {
            cptNote = uneNote;
        }
        public string GetNote()
        {
            return lbLaNote.Text;
        }
        public void PlusUn()
        {
            Eleve.AjouterUnPoint();
            lbLaNote.Text = Convert.ToString(Eleve.GetNoteEleve());
        }
        public void MoinsUn()
        {
            Eleve.RetirerUnPoint();
            lbLaNote.Text = Convert.ToString(Eleve.GetNoteEleve());
        }
        public void CacherLesControls()
        {
            btValiderNom.Hide();
            lbSaisirNom.Hide();
            tbNom.Hide();
        }
        public void AfficherLesControls()
        {
            btValiderNom.Show();
            lbSaisirNom.Show();
            tbNom.Show();
        }
        public void ViderLeChampNom()
        {
            lbNomEleve.Text = "";
        }

        private void pictureBox1_Click(object sender, EventArgs e) //permet a l'utilisateur de selectionner une image pour un éleve.
        {
            OpenFileDialog ouvrirFichier = new OpenFileDialog();  //permet de parcourir les fichiers du PC pour ajouter une image
            ouvrirFichier.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (ouvrirFichier.ShowDialog() == DialogResult.OK)
            {
                pBImageProfEleve.Image = new Bitmap(ouvrirFichier.FileName);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btHisto_Click(object sender, EventArgs e)
        {
            string pos = "";
            string neg = "";
            int cmpt = 1;
            foreach (string s in Commentaireplus.commPlus)
            {
                pos = pos + cmpt + ". " + s + "\n";
                cmpt = cmpt + 1;
            }
            cmpt = 1;
            foreach (string t in Commentairemoins.commMoins)
            {
                neg = neg + cmpt + ". " + t + "\n";
                cmpt = cmpt + 1;
            }
            MessageBox.Show("Bon Commentaire :\n \n" + pos + "\nMauvais Commentaire :\n \n" + neg);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            lbNomEleve.Visible = true;
            pBImageProfEleve.Visible = true;
            lbNote.Visible = true;
            lbLaNote.Visible = true;
            btMoins.Visible = true;
            btPlus.Visible = true;
            btHisto.Visible = true;
            lbSaisirNom.Visible = true;
            tbNom.Visible = true;
            btValiderNom.Visible = true;
            btajouter.Visible = false;
            Eleve.SetNoteEleve(0);
            lbLaNote.Text = "0";
        }

        private void lbLaNote_Click(object sender, EventArgs e)
        {

        }
    }
}
