﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class Commentairemoins : Form
    {
        public ArrayList commMoins = new ArrayList();
        public Commentairemoins()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radio1.Checked == true)
            {
                commMoins.Add("Pour dispersion. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio2.Checked == true)
            {
                commMoins.Add("Pour ne pas avoir suivi. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio3.Checked == true)
            {
                commMoins.Add("Pour bavardage. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio4.Checked == true)
            {
                commMoins.Add("Pour irrespect. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio5.Checked == true)
            {
                commMoins.Add("Pour retard. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radioautre.Checked == true)
            {
                commMoins.Add(richtbautre.Text + "Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            //MessageBox.Show(Convert.ToString(commmoins.Count)); pour vérifier que les commentaires s'ajoutent bien dans l'arraylist
            this.Hide();
        }

        private void radio1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
