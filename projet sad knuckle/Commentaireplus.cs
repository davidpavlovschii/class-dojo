﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class Commentaireplus : Form
    {
        public ArrayList commPlus = new ArrayList();
        public Commentaireplus()
        {
            InitializeComponent();
        }

        private void btvalider_Click(object sender, EventArgs e)
        {
            if (radio1.Checked == true)
            {
                commPlus.Add("Pour bon comportement. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio2.Checked == true)
            {
                commPlus.Add("Pour bonne réponse méritante. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio3.Checked == true)
            {
                commPlus.Add("Pour amélioration. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio4.Checked == true)
            {
                commPlus.Add("Pour acte de noblesse. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radio5.Checked == true)
            {
                commPlus.Add("Pour respect. Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            if (radioautre.Checked == true)
            {
                commPlus.Add(richtbautre.Text + "Le " + DateTime.Now.ToShortDateString() + " à " + DateTime.Now.ToShortTimeString());
            }
            //MessageBox.Show(Convert.ToString(commplus.Count)); pour vérifier que les commentaires s'ajoutent bien dans l'arraylist
            this.Hide();
        }

    }
}
