﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    partial class ClasseDojo : Form
    {
        CommentairemoinsTous CommMoins = new CommentairemoinsTous();
        CommentaireplusTous CommPlus = new CommentaireplusTous();
        ArrayList lesSections = new ArrayList();
        Section uneSection = new Section(Section.GetListEleve());
        JeuPendu leFameuxPendu = new JeuPendu();
        public ClasseDojo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (uneSection == null)
                SaisirNomSection();
            else
            {
                uneSection.PlusUnToutLeMonde();
                CommPlus.Show();
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (uneSection == null)
                SaisirNomSection();
            else
            {
                uneSection.MoinsUnToutLeMonde();
                CommMoins.Show();
            }

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (uneSection == null)
                SaisirNomSection();
            else
                uneSection.InterrogationAleatoire();  
        }

        private void button4_Click(object sender, EventArgs e)
        {
            btAfficherSectionEnregistree.Visible = true;
            uneSection = new Section(tbNomNouvelleSection.Text);
            lesSections.Add(uneSection);
            gbSection.Text = tbNomNouvelleSection.Text;
            foreach (ProfilEleve unEleve in panSection.Controls)
            {
                Section.AjouterProfEleve(unEleve);
            }
            uneSection.EnregistrerLaSection();
            cbLesSections.Items.Add(uneSection.GetNomSection());
            cbLesSections.Text = tbNomNouvelleSection.Text;
            foreach (Control unControl in panInteraction.Controls)
            {
                unControl.Visible = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            uneSection.AfficherLesEleves();
        }
        private void SaisirNomSection()
        {
            MessageBox.Show("Veuillez d'abord créer une section");
        }

        private void cbLesSections_SelectedIndexChanged(object sender, EventArgs e)
        {
            gbSection.Text = Convert.ToString(cbLesSections.SelectedItem);
        }

        private void btJeu_Click(object sender, EventArgs e)
        {
            // show other form

            leFameuxPendu.ShowDialog();
        }

        private void btQuitter_Click(object sender, EventArgs e)
        {
            FileStream f = new FileStream("lesEleves.txt", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(f, Section.GetListEleve());
            f.Close();
            Application.Exit();
        }
        private void btDeserialiser_Click(object sender, EventArgs e)
        {
            FileStream f = new FileStream("lesEleves.txt", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            Section.SetListEleve((List<Eleve>)bf.Deserialize(f));
            profEleve1.SetEleve(uneSection.GetUnEleve(0));
            f.Close();
        }

        private void panInteraction_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click_2(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(Convert.ToString(Section.CompterLesEleves()));
        }

        private void Affichermauvaiscomm_Click(object sender, EventArgs e)
        {
            string commentaireneg = "";
            foreach (string t in CommMoins.CommMoins)
            {
                commentaireneg = commentaireneg + t + "\n";
            }
            MessageBox.Show(commentaireneg);
        }

        private void Afficherboncomm_Click_1(object sender, EventArgs e)
        {
            string commentairepos = "";
            foreach (string s in CommPlus.CommPlus)
            {
                commentairepos = commentairepos + s + "\n";
            }
            MessageBox.Show(commentairepos);
        }
    }
}
