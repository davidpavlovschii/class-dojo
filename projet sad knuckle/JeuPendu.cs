﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class JeuPendu : Form
    {
        int noImage = 0;
        string[] lesMots = { "blyat", "cyka", "idinahui", "pidar", "gandon", "cytchka" };
        string uneLettre;
        Random rd = new Random();
        int motAleat;
        string unMot;
        public JeuPendu()
        {
            InitializeComponent();
        }
        public void lettreAlphabetClique(Button sender)
        {
            uneLettre = (string)sender.Tag;
            label1.Text = uneLettre;
            unMot = lesMots[motAleat];
            label2.Text = unMot;
            if (unMot.ToLower().Contains(uneLettre.ToLower()))
            {
                sender.Enabled = false;
                sender.BackColor = Color.Green;
            }
            else
            {
                sender.Enabled = false;
                sender.BackColor = Color.Red;
                noImage++;
                pictureBox1.Image = imageList1.Images[noImage];
                if (noImage >= 10)
                {
                    MessageBox.Show("vous avez perdu ! ");
                    this.Hide();
                    this.Close();
                    noImage = 0;
                    pictureBox1.Image = imageList1.Images[noImage];
                }
            }      
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            motAleat = rd.Next(0, lesMots.Length);
            unMot = lesMots[motAleat];
            char[] lesLettres = unMot.ToCharArray();
            foreach (char uneLettre in lesLettres)
            {

            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            noImage = 0;
            pictureBox1.Image = imageList1.Images[noImage];
        }

        private void btA_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btA);
        }

        private void btB_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btB);
        }

        private void btC_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btC);
        }

        private void btD_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btD);
        }

        private void btK_Click_1(object sender, EventArgs e)
        {
            lettreAlphabetClique(btK);
        }

        private void btE_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btE);
        }

        private void btF_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btF);
        }

        private void btG_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btG);
        }

        private void btH_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btH);
        }

        private void btI_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btI);
        }

        private void btJ_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btJ);
        }

        private void btL_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btL);
        }

        private void btM_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btM);
        }

        private void btN_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btN);
        }

        private void btO_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btO);
        }

        private void btP_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btP);
        }

        private void btQ_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btQ);
        }

        private void btR_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btR);
        }

        private void btS_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btS);
        }

        private void btT_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btT);
        }

        private void btU_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btU);
        }

        private void btV_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btV);
        }

        private void btW_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btW);
        }

        private void btX_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btX);
        }

        private void btY_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btY);
        }

        private void btZ_Click(object sender, EventArgs e)
        {
            lettreAlphabetClique(btZ);
        }
    }
}
