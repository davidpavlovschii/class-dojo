﻿namespace projet_sad_knuckle
{
    partial class ClasseDojo
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClasseDojo));
            this.btPlusTous = new System.Windows.Forms.Button();
            this.btMoinsTous = new System.Windows.Forms.Button();
            this.btInterroAleat = new System.Windows.Forms.Button();
            this.btCreerUneNouvelleSection = new System.Windows.Forms.Button();
            this.btAfficherSectionEnregistree = new System.Windows.Forms.Button();
            this.cbLesSections = new System.Windows.Forms.ComboBox();
            this.tbNomNouvelleSection = new System.Windows.Forms.TextBox();
            this.lbNomSection = new System.Windows.Forms.Label();
            this.panSection = new System.Windows.Forms.Panel();
            this.gbSection = new System.Windows.Forms.Label();
            this.lbLesSections = new System.Windows.Forms.Label();
            this.panInteraction = new System.Windows.Forms.Panel();
            this.btJeu = new System.Windows.Forms.Button();
            this.btQuitter = new System.Windows.Forms.Button();
            this.btDeserialiser = new System.Windows.Forms.Button();
            this.Affichermauvaiscomm = new System.Windows.Forms.Button();
            this.Afficherboncomm = new System.Windows.Forms.Button();
            this.gbcontrols = new System.Windows.Forms.GroupBox();
            this.profEleve12 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve11 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve10 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve9 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve8 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve7 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve6 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve5 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve4 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve3 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve2 = new projet_sad_knuckle.ProfilEleve();
            this.profEleve1 = new projet_sad_knuckle.ProfilEleve();
            this.panSection.SuspendLayout();
            this.panInteraction.SuspendLayout();
            this.gbcontrols.SuspendLayout();
            this.SuspendLayout();
            // 
            // btPlusTous
            // 
            this.btPlusTous.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPlusTous.Image = ((System.Drawing.Image)(resources.GetObject("btPlusTous.Image")));
            this.btPlusTous.Location = new System.Drawing.Point(5, 54);
            this.btPlusTous.Name = "btPlusTous";
            this.btPlusTous.Size = new System.Drawing.Size(48, 35);
            this.btPlusTous.TabIndex = 10;
            this.btPlusTous.Text = "+";
            this.btPlusTous.UseVisualStyleBackColor = true;
            this.btPlusTous.Visible = false;
            this.btPlusTous.Click += new System.EventHandler(this.button1_Click);
            // 
            // btMoinsTous
            // 
            this.btMoinsTous.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMoinsTous.Image = ((System.Drawing.Image)(resources.GetObject("btMoinsTous.Image")));
            this.btMoinsTous.Location = new System.Drawing.Point(236, 54);
            this.btMoinsTous.Name = "btMoinsTous";
            this.btMoinsTous.Size = new System.Drawing.Size(48, 35);
            this.btMoinsTous.TabIndex = 11;
            this.btMoinsTous.Text = "-";
            this.btMoinsTous.UseVisualStyleBackColor = true;
            this.btMoinsTous.Visible = false;
            this.btMoinsTous.Click += new System.EventHandler(this.button2_Click);
            // 
            // btInterroAleat
            // 
            this.btInterroAleat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInterroAleat.Location = new System.Drawing.Point(57, 54);
            this.btInterroAleat.Name = "btInterroAleat";
            this.btInterroAleat.Size = new System.Drawing.Size(173, 35);
            this.btInterroAleat.TabIndex = 13;
            this.btInterroAleat.Text = "Interroger un eleve au hasard";
            this.btInterroAleat.UseVisualStyleBackColor = true;
            this.btInterroAleat.Visible = false;
            this.btInterroAleat.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // btCreerUneNouvelleSection
            // 
            this.btCreerUneNouvelleSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCreerUneNouvelleSection.Location = new System.Drawing.Point(4, 36);
            this.btCreerUneNouvelleSection.Name = "btCreerUneNouvelleSection";
            this.btCreerUneNouvelleSection.Size = new System.Drawing.Size(123, 46);
            this.btCreerUneNouvelleSection.TabIndex = 14;
            this.btCreerUneNouvelleSection.Text = "Créer une nouvelle section";
            this.btCreerUneNouvelleSection.UseVisualStyleBackColor = true;
            this.btCreerUneNouvelleSection.Click += new System.EventHandler(this.button4_Click);
            // 
            // btAfficherSectionEnregistree
            // 
            this.btAfficherSectionEnregistree.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAfficherSectionEnregistree.Location = new System.Drawing.Point(172, 36);
            this.btAfficherSectionEnregistree.Name = "btAfficherSectionEnregistree";
            this.btAfficherSectionEnregistree.Size = new System.Drawing.Size(123, 46);
            this.btAfficherSectionEnregistree.TabIndex = 15;
            this.btAfficherSectionEnregistree.Text = "Afficher les eleves enregistrés";
            this.btAfficherSectionEnregistree.UseVisualStyleBackColor = true;
            this.btAfficherSectionEnregistree.Visible = false;
            this.btAfficherSectionEnregistree.Click += new System.EventHandler(this.button5_Click);
            // 
            // cbLesSections
            // 
            this.cbLesSections.FormattingEnabled = true;
            this.cbLesSections.Location = new System.Drawing.Point(85, 19);
            this.cbLesSections.Name = "cbLesSections";
            this.cbLesSections.Size = new System.Drawing.Size(121, 21);
            this.cbLesSections.TabIndex = 16;
            this.cbLesSections.Visible = false;
            this.cbLesSections.SelectedIndexChanged += new System.EventHandler(this.cbLesSections_SelectedIndexChanged);
            // 
            // tbNomNouvelleSection
            // 
            this.tbNomNouvelleSection.Location = new System.Drawing.Point(90, 11);
            this.tbNomNouvelleSection.Name = "tbNomNouvelleSection";
            this.tbNomNouvelleSection.Size = new System.Drawing.Size(84, 20);
            this.tbNomNouvelleSection.TabIndex = 18;
            // 
            // lbNomSection
            // 
            this.lbNomSection.AutoSize = true;
            this.lbNomSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNomSection.Location = new System.Drawing.Point(4, 11);
            this.lbNomSection.Name = "lbNomSection";
            this.lbNomSection.Size = new System.Drawing.Size(87, 15);
            this.lbNomSection.TabIndex = 19;
            this.lbNomSection.Text = "Nom Section : ";
            // 
            // panSection
            // 
            this.panSection.BackColor = System.Drawing.Color.Transparent;
            this.panSection.Controls.Add(this.profEleve12);
            this.panSection.Controls.Add(this.profEleve11);
            this.panSection.Controls.Add(this.profEleve10);
            this.panSection.Controls.Add(this.profEleve9);
            this.panSection.Controls.Add(this.profEleve8);
            this.panSection.Controls.Add(this.profEleve7);
            this.panSection.Controls.Add(this.profEleve6);
            this.panSection.Controls.Add(this.profEleve5);
            this.panSection.Controls.Add(this.profEleve4);
            this.panSection.Controls.Add(this.profEleve3);
            this.panSection.Controls.Add(this.profEleve2);
            this.panSection.Controls.Add(this.profEleve1);
            this.panSection.Location = new System.Drawing.Point(12, 46);
            this.panSection.Name = "panSection";
            this.panSection.Size = new System.Drawing.Size(950, 531);
            this.panSection.TabIndex = 20;
            // 
            // gbSection
            // 
            this.gbSection.AutoSize = true;
            this.gbSection.BackColor = System.Drawing.Color.Transparent;
            this.gbSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSection.Location = new System.Drawing.Point(14, 15);
            this.gbSection.Name = "gbSection";
            this.gbSection.Size = new System.Drawing.Size(100, 20);
            this.gbSection.TabIndex = 49;
            this.gbSection.Text = "Nom Section";
            // 
            // lbLesSections
            // 
            this.lbLesSections.AutoSize = true;
            this.lbLesSections.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLesSections.Location = new System.Drawing.Point(3, 22);
            this.lbLesSections.Name = "lbLesSections";
            this.lbLesSections.Size = new System.Drawing.Size(84, 15);
            this.lbLesSections.TabIndex = 50;
            this.lbLesSections.Text = "Vos sections : ";
            this.lbLesSections.Visible = false;
            // 
            // panInteraction
            // 
            this.panInteraction.Controls.Add(this.cbLesSections);
            this.panInteraction.Controls.Add(this.lbLesSections);
            this.panInteraction.Controls.Add(this.btPlusTous);
            this.panInteraction.Controls.Add(this.btMoinsTous);
            this.panInteraction.Controls.Add(this.btInterroAleat);
            this.panInteraction.Location = new System.Drawing.Point(4, 89);
            this.panInteraction.Name = "panInteraction";
            this.panInteraction.Size = new System.Drawing.Size(291, 100);
            this.panInteraction.TabIndex = 51;
            this.panInteraction.Paint += new System.Windows.Forms.PaintEventHandler(this.panInteraction_Paint);
            // 
            // btJeu
            // 
            this.btJeu.Location = new System.Drawing.Point(4, 195);
            this.btJeu.Name = "btJeu";
            this.btJeu.Size = new System.Drawing.Size(123, 41);
            this.btJeu.TabIndex = 52;
            this.btJeu.Text = "Jeu du pendu";
            this.btJeu.UseVisualStyleBackColor = true;
            this.btJeu.Click += new System.EventHandler(this.btJeu_Click);
            // 
            // btQuitter
            // 
            this.btQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btQuitter.Image = ((System.Drawing.Image)(resources.GetObject("btQuitter.Image")));
            this.btQuitter.Location = new System.Drawing.Point(201, 490);
            this.btQuitter.Name = "btQuitter";
            this.btQuitter.Size = new System.Drawing.Size(96, 40);
            this.btQuitter.TabIndex = 53;
            this.btQuitter.Text = "Quitter";
            this.btQuitter.UseVisualStyleBackColor = true;
            this.btQuitter.Click += new System.EventHandler(this.btQuitter_Click);
            // 
            // btDeserialiser
            // 
            this.btDeserialiser.Location = new System.Drawing.Point(172, 195);
            this.btDeserialiser.Name = "btDeserialiser";
            this.btDeserialiser.Size = new System.Drawing.Size(124, 41);
            this.btDeserialiser.TabIndex = 61;
            this.btDeserialiser.Text = "Afficher classe déjà créée";
            this.btDeserialiser.UseVisualStyleBackColor = true;
            this.btDeserialiser.Click += new System.EventHandler(this.btDeserialiser_Click);
            // 
            // Affichermauvaiscomm
            // 
            this.Affichermauvaiscomm.Location = new System.Drawing.Point(172, 314);
            this.Affichermauvaiscomm.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Affichermauvaiscomm.Name = "Affichermauvaiscomm";
            this.Affichermauvaiscomm.Size = new System.Drawing.Size(124, 46);
            this.Affichermauvaiscomm.TabIndex = 63;
            this.Affichermauvaiscomm.Text = "Afficher les mauvais commentaires.";
            this.Affichermauvaiscomm.UseVisualStyleBackColor = true;
            this.Affichermauvaiscomm.Click += new System.EventHandler(this.Affichermauvaiscomm_Click);
            // 
            // Afficherboncomm
            // 
            this.Afficherboncomm.Location = new System.Drawing.Point(6, 314);
            this.Afficherboncomm.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Afficherboncomm.Name = "Afficherboncomm";
            this.Afficherboncomm.Size = new System.Drawing.Size(123, 46);
            this.Afficherboncomm.TabIndex = 64;
            this.Afficherboncomm.Text = "Afficher les bons commentaires.";
            this.Afficherboncomm.UseVisualStyleBackColor = true;
            this.Afficherboncomm.Click += new System.EventHandler(this.Afficherboncomm_Click_1);
            // 
            // gbcontrols
            // 
            this.gbcontrols.BackColor = System.Drawing.Color.Transparent;
            this.gbcontrols.Controls.Add(this.Afficherboncomm);
            this.gbcontrols.Controls.Add(this.Affichermauvaiscomm);
            this.gbcontrols.Controls.Add(this.btDeserialiser);
            this.gbcontrols.Controls.Add(this.btQuitter);
            this.gbcontrols.Controls.Add(this.btJeu);
            this.gbcontrols.Controls.Add(this.panInteraction);
            this.gbcontrols.Controls.Add(this.lbNomSection);
            this.gbcontrols.Controls.Add(this.tbNomNouvelleSection);
            this.gbcontrols.Controls.Add(this.btAfficherSectionEnregistree);
            this.gbcontrols.Controls.Add(this.btCreerUneNouvelleSection);
            this.gbcontrols.Location = new System.Drawing.Point(975, 41);
            this.gbcontrols.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbcontrols.Name = "gbcontrols";
            this.gbcontrols.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbcontrols.Size = new System.Drawing.Size(303, 536);
            this.gbcontrols.TabIndex = 65;
            this.gbcontrols.TabStop = false;
            // 
            // profEleve12
            // 
            this.profEleve12.Location = new System.Drawing.Point(680, 351);
            this.profEleve12.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve12.Name = "profEleve12";
            this.profEleve12.Size = new System.Drawing.Size(219, 167);
            this.profEleve12.TabIndex = 48;
            this.profEleve12.Tag = "12";
            // 
            // profEleve11
            // 
            this.profEleve11.Location = new System.Drawing.Point(455, 351);
            this.profEleve11.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve11.Name = "profEleve11";
            this.profEleve11.Size = new System.Drawing.Size(219, 167);
            this.profEleve11.TabIndex = 47;
            this.profEleve11.Tag = "11";
            // 
            // profEleve10
            // 
            this.profEleve10.Location = new System.Drawing.Point(230, 351);
            this.profEleve10.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve10.Name = "profEleve10";
            this.profEleve10.Size = new System.Drawing.Size(219, 167);
            this.profEleve10.TabIndex = 46;
            this.profEleve10.Tag = "10";
            // 
            // profEleve9
            // 
            this.profEleve9.Location = new System.Drawing.Point(5, 351);
            this.profEleve9.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve9.Name = "profEleve9";
            this.profEleve9.Size = new System.Drawing.Size(219, 167);
            this.profEleve9.TabIndex = 45;
            this.profEleve9.Tag = "9";
            // 
            // profEleve8
            // 
            this.profEleve8.Location = new System.Drawing.Point(680, 178);
            this.profEleve8.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve8.Name = "profEleve8";
            this.profEleve8.Size = new System.Drawing.Size(219, 167);
            this.profEleve8.TabIndex = 44;
            this.profEleve8.Tag = "8";
            // 
            // profEleve7
            // 
            this.profEleve7.Location = new System.Drawing.Point(680, 3);
            this.profEleve7.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve7.Name = "profEleve7";
            this.profEleve7.Size = new System.Drawing.Size(219, 167);
            this.profEleve7.TabIndex = 43;
            this.profEleve7.Tag = "4";
            // 
            // profEleve6
            // 
            this.profEleve6.Location = new System.Drawing.Point(455, 178);
            this.profEleve6.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve6.Name = "profEleve6";
            this.profEleve6.Size = new System.Drawing.Size(219, 167);
            this.profEleve6.TabIndex = 42;
            this.profEleve6.Tag = "7";
            // 
            // profEleve5
            // 
            this.profEleve5.Location = new System.Drawing.Point(230, 178);
            this.profEleve5.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve5.Name = "profEleve5";
            this.profEleve5.Size = new System.Drawing.Size(219, 167);
            this.profEleve5.TabIndex = 41;
            this.profEleve5.Tag = "6";
            // 
            // profEleve4
            // 
            this.profEleve4.Location = new System.Drawing.Point(5, 178);
            this.profEleve4.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve4.Name = "profEleve4";
            this.profEleve4.Size = new System.Drawing.Size(219, 167);
            this.profEleve4.TabIndex = 40;
            this.profEleve4.Tag = "5";
            // 
            // profEleve3
            // 
            this.profEleve3.Location = new System.Drawing.Point(455, 3);
            this.profEleve3.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve3.Name = "profEleve3";
            this.profEleve3.Size = new System.Drawing.Size(219, 167);
            this.profEleve3.TabIndex = 39;
            this.profEleve3.Tag = "3";
            // 
            // profEleve2
            // 
            this.profEleve2.Location = new System.Drawing.Point(5, 3);
            this.profEleve2.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve2.Name = "profEleve2";
            this.profEleve2.Size = new System.Drawing.Size(219, 167);
            this.profEleve2.TabIndex = 38;
            this.profEleve2.Tag = "1";
            // 
            // profEleve1
            // 
            this.profEleve1.Location = new System.Drawing.Point(230, 3);
            this.profEleve1.Margin = new System.Windows.Forms.Padding(4);
            this.profEleve1.Name = "profEleve1";
            this.profEleve1.Size = new System.Drawing.Size(219, 167);
            this.profEleve1.TabIndex = 37;
            this.profEleve1.Tag = "2";
            // 
            // ClasseDojo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.BackgroundImage = global::projet_sad_knuckle.Properties.Resources.fond_interface;
            this.ClientSize = new System.Drawing.Size(1278, 586);
            this.Controls.Add(this.gbcontrols);
            this.Controls.Add(this.gbSection);
            this.Controls.Add(this.panSection);
            this.Name = "ClasseDojo";
            this.Text = "Good or Bad Students";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panSection.ResumeLayout(false);
            this.panInteraction.ResumeLayout(false);
            this.panInteraction.PerformLayout();
            this.gbcontrols.ResumeLayout(false);
            this.gbcontrols.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btPlusTous;
        private System.Windows.Forms.Button btMoinsTous;
        private System.Windows.Forms.Button btInterroAleat;
        private System.Windows.Forms.Button btCreerUneNouvelleSection;
        private System.Windows.Forms.Button btAfficherSectionEnregistree;
        private System.Windows.Forms.ComboBox cbLesSections;
        private System.Windows.Forms.TextBox tbNomNouvelleSection;
        private System.Windows.Forms.Label lbNomSection;
        private System.Windows.Forms.Panel panSection;
        private ProfilEleve profEleve12;
        private ProfilEleve profEleve11;
        private ProfilEleve profEleve10;
        private ProfilEleve profEleve9;
        private ProfilEleve profEleve8;
        private ProfilEleve profEleve7;
        private ProfilEleve profEleve6;
        private ProfilEleve profEleve5;
        private ProfilEleve profEleve4;
        private ProfilEleve profEleve3;
        private ProfilEleve profEleve2;
        private ProfilEleve profEleve1;
        private System.Windows.Forms.Label gbSection;
        private System.Windows.Forms.Label lbLesSections;
        private System.Windows.Forms.Panel panInteraction;
        private System.Windows.Forms.Button btJeu;
        private System.Windows.Forms.Button btQuitter;
        private System.Windows.Forms.Button btDeserialiser;
        private System.Windows.Forms.Button Affichermauvaiscomm;
        private System.Windows.Forms.Button Afficherboncomm;
        private System.Windows.Forms.GroupBox gbcontrols;
    }
}

