﻿namespace projet_sad_knuckle
{
    partial class Commentairemoins
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radio1 = new System.Windows.Forms.RadioButton();
            this.radio2 = new System.Windows.Forms.RadioButton();
            this.radio3 = new System.Windows.Forms.RadioButton();
            this.radio4 = new System.Windows.Forms.RadioButton();
            this.radio5 = new System.Windows.Forms.RadioButton();
            this.radioautre = new System.Windows.Forms.RadioButton();
            this.richtbautre = new System.Windows.Forms.RichTextBox();
            this.btvalider = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Veuillez justifier le retrait de point pour l\'élève.";
            // 
            // radio1
            // 
            this.radio1.AutoSize = true;
            this.radio1.Location = new System.Drawing.Point(36, 62);
            this.radio1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio1.Name = "radio1";
            this.radio1.Size = new System.Drawing.Size(85, 21);
            this.radio1.TabIndex = 1;
            this.radio1.TabStop = true;
            this.radio1.Text = "Dispersé";
            this.radio1.UseVisualStyleBackColor = true;
            this.radio1.CheckedChanged += new System.EventHandler(this.radio1_CheckedChanged);
            // 
            // radio2
            // 
            this.radio2.AutoSize = true;
            this.radio2.Location = new System.Drawing.Point(36, 111);
            this.radio2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio2.Name = "radio2";
            this.radio2.Size = new System.Drawing.Size(103, 21);
            this.radio2.TabIndex = 2;
            this.radio2.TabStop = true;
            this.radio2.Text = "Ne suis pas";
            this.radio2.UseVisualStyleBackColor = true;
            // 
            // radio3
            // 
            this.radio3.AutoSize = true;
            this.radio3.Location = new System.Drawing.Point(36, 167);
            this.radio3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio3.Name = "radio3";
            this.radio3.Size = new System.Drawing.Size(98, 21);
            this.radio3.TabIndex = 3;
            this.radio3.TabStop = true;
            this.radio3.Text = "Bavardage";
            this.radio3.UseVisualStyleBackColor = true;
            // 
            // radio4
            // 
            this.radio4.AutoSize = true;
            this.radio4.Location = new System.Drawing.Point(36, 223);
            this.radio4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio4.Name = "radio4";
            this.radio4.Size = new System.Drawing.Size(114, 21);
            this.radio4.TabIndex = 4;
            this.radio4.TabStop = true;
            this.radio4.Text = "Irrespectueux";
            this.radio4.UseVisualStyleBackColor = true;
            // 
            // radio5
            // 
            this.radio5.AutoSize = true;
            this.radio5.Location = new System.Drawing.Point(36, 278);
            this.radio5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio5.Name = "radio5";
            this.radio5.Size = new System.Drawing.Size(72, 21);
            this.radio5.TabIndex = 5;
            this.radio5.TabStop = true;
            this.radio5.Text = "Retard";
            this.radio5.UseVisualStyleBackColor = true;
            // 
            // radioautre
            // 
            this.radioautre.AutoSize = true;
            this.radioautre.Location = new System.Drawing.Point(36, 335);
            this.radioautre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioautre.Name = "radioautre";
            this.radioautre.Size = new System.Drawing.Size(71, 21);
            this.radioautre.TabIndex = 6;
            this.radioautre.TabStop = true;
            this.radioautre.Text = "Autre :";
            this.radioautre.UseVisualStyleBackColor = true;
            // 
            // richtbautre
            // 
            this.richtbautre.Location = new System.Drawing.Point(113, 335);
            this.richtbautre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richtbautre.Name = "richtbautre";
            this.richtbautre.Size = new System.Drawing.Size(200, 75);
            this.richtbautre.TabIndex = 7;
            this.richtbautre.Text = "";
            // 
            // btvalider
            // 
            this.btvalider.Location = new System.Drawing.Point(113, 434);
            this.btvalider.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btvalider.Name = "btvalider";
            this.btvalider.Size = new System.Drawing.Size(125, 45);
            this.btvalider.TabIndex = 8;
            this.btvalider.Text = "Valider le retrait";
            this.btvalider.UseVisualStyleBackColor = true;
            this.btvalider.Click += new System.EventHandler(this.button1_Click);
            // 
            // Commentairemoins
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 503);
            this.Controls.Add(this.btvalider);
            this.Controls.Add(this.richtbautre);
            this.Controls.Add(this.radioautre);
            this.Controls.Add(this.radio5);
            this.Controls.Add(this.radio4);
            this.Controls.Add(this.radio3);
            this.Controls.Add(this.radio2);
            this.Controls.Add(this.radio1);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Commentairemoins";
            this.Text = "Justification du retrait";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radio1;
        private System.Windows.Forms.RadioButton radio2;
        private System.Windows.Forms.RadioButton radio3;
        private System.Windows.Forms.RadioButton radio4;
        private System.Windows.Forms.RadioButton radio5;
        private System.Windows.Forms.RadioButton radioautre;
        private System.Windows.Forms.RichTextBox richtbautre;
        private System.Windows.Forms.Button btvalider;
    }
}