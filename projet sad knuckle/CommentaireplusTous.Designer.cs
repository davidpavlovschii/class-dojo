﻿namespace projet_sad_knuckle
{
    partial class CommentaireplusTous
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radio1 = new System.Windows.Forms.RadioButton();
            this.radio2 = new System.Windows.Forms.RadioButton();
            this.radio3 = new System.Windows.Forms.RadioButton();
            this.radioautre = new System.Windows.Forms.RadioButton();
            this.richtbautre = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Veuillez justifier l\'ajout du point pour la classe...";
            // 
            // radio1
            // 
            this.radio1.AutoSize = true;
            this.radio1.Location = new System.Drawing.Point(36, 62);
            this.radio1.Name = "radio1";
            this.radio1.Size = new System.Drawing.Size(112, 21);
            this.radio1.TabIndex = 2;
            this.radio1.TabStop = true;
            this.radio1.Text = "Classe calme";
            this.radio1.UseVisualStyleBackColor = true;
            // 
            // radio2
            // 
            this.radio2.AutoSize = true;
            this.radio2.Location = new System.Drawing.Point(36, 129);
            this.radio2.Name = "radio2";
            this.radio2.Size = new System.Drawing.Size(212, 21);
            this.radio2.TabIndex = 3;
            this.radio2.TabStop = true;
            this.radio2.Text = "Bonne écoute des consignes";
            this.radio2.UseVisualStyleBackColor = true;
            // 
            // radio3
            // 
            this.radio3.AutoSize = true;
            this.radio3.Location = new System.Drawing.Point(36, 200);
            this.radio3.Name = "radio3";
            this.radio3.Size = new System.Drawing.Size(190, 21);
            this.radio3.TabIndex = 4;
            this.radio3.TabStop = true;
            this.radio3.Text = "Respect d\'autrui constant";
            this.radio3.UseVisualStyleBackColor = true;
            // 
            // radioautre
            // 
            this.radioautre.AutoSize = true;
            this.radioautre.Location = new System.Drawing.Point(36, 269);
            this.radioautre.Name = "radioautre";
            this.radioautre.Size = new System.Drawing.Size(71, 21);
            this.radioautre.TabIndex = 5;
            this.radioautre.TabStop = true;
            this.radioautre.Text = "Autre :";
            this.radioautre.UseVisualStyleBackColor = true;
            // 
            // richtbautre
            // 
            this.richtbautre.Location = new System.Drawing.Point(113, 268);
            this.richtbautre.Name = "richtbautre";
            this.richtbautre.Size = new System.Drawing.Size(200, 75);
            this.richtbautre.TabIndex = 6;
            this.richtbautre.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(113, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 45);
            this.button1.TabIndex = 7;
            this.button1.Text = " Valider le retrait";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CommentaireplusTous
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 428);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richtbautre);
            this.Controls.Add(this.radioautre);
            this.Controls.Add(this.radio3);
            this.Controls.Add(this.radio2);
            this.Controls.Add(this.radio1);
            this.Controls.Add(this.label1);
            this.Name = "CommentaireplusTous";
            this.Text = "CommentaireplusTous";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radio1;
        private System.Windows.Forms.RadioButton radio2;
        private System.Windows.Forms.RadioButton radio3;
        private System.Windows.Forms.RadioButton radioautre;
        private System.Windows.Forms.RichTextBox richtbautre;
        private System.Windows.Forms.Button button1;
    }
}