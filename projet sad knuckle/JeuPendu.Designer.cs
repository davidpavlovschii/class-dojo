﻿namespace projet_sad_knuckle
{
    partial class JeuPendu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JeuPendu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button27 = new System.Windows.Forms.Button();
            this.btZ = new System.Windows.Forms.Button();
            this.btY = new System.Windows.Forms.Button();
            this.btX = new System.Windows.Forms.Button();
            this.btW = new System.Windows.Forms.Button();
            this.btV = new System.Windows.Forms.Button();
            this.btU = new System.Windows.Forms.Button();
            this.btT = new System.Windows.Forms.Button();
            this.btS = new System.Windows.Forms.Button();
            this.btR = new System.Windows.Forms.Button();
            this.btQ = new System.Windows.Forms.Button();
            this.btP = new System.Windows.Forms.Button();
            this.btO = new System.Windows.Forms.Button();
            this.btN = new System.Windows.Forms.Button();
            this.btM = new System.Windows.Forms.Button();
            this.btL = new System.Windows.Forms.Button();
            this.btK = new System.Windows.Forms.Button();
            this.btJ = new System.Windows.Forms.Button();
            this.btI = new System.Windows.Forms.Button();
            this.btH = new System.Windows.Forms.Button();
            this.btG = new System.Windows.Forms.Button();
            this.btF = new System.Windows.Forms.Button();
            this.btE = new System.Windows.Forms.Button();
            this.btD = new System.Windows.Forms.Button();
            this.btC = new System.Windows.Forms.Button();
            this.btB = new System.Windows.Forms.Button();
            this.btA = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::projet_sad_knuckle.Properties.Resources.pendu_1;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(155, 152);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pendu 1.PNG");
            this.imageList1.Images.SetKeyName(1, "pendu 2.PNG");
            this.imageList1.Images.SetKeyName(2, "pendu 3.PNG");
            this.imageList1.Images.SetKeyName(3, "pendu 4.PNG");
            this.imageList1.Images.SetKeyName(4, "pendu 5.PNG");
            this.imageList1.Images.SetKeyName(5, "pendu 6.PNG");
            this.imageList1.Images.SetKeyName(6, "pendu 7.PNG");
            this.imageList1.Images.SetKeyName(7, "pendu 8.PNG");
            this.imageList1.Images.SetKeyName(8, "pendu 9.PNG");
            this.imageList1.Images.SetKeyName(9, "pendu 10.PNG");
            this.imageList1.Images.SetKeyName(10, "pendu 11.PNG");
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(192, 17);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(92, 23);
            this.button27.TabIndex = 26;
            this.button27.Text = "recommencer";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // btZ
            // 
            this.btZ.Location = new System.Drawing.Point(425, 290);
            this.btZ.Name = "btZ";
            this.btZ.Size = new System.Drawing.Size(27, 23);
            this.btZ.TabIndex = 106;
            this.btZ.Tag = "Z";
            this.btZ.Text = "Z";
            this.btZ.UseVisualStyleBackColor = true;
            this.btZ.Click += new System.EventHandler(this.btZ_Click);
            // 
            // btY
            // 
            this.btY.Location = new System.Drawing.Point(392, 290);
            this.btY.Name = "btY";
            this.btY.Size = new System.Drawing.Size(27, 23);
            this.btY.TabIndex = 105;
            this.btY.Tag = "Y";
            this.btY.Text = "Y";
            this.btY.UseVisualStyleBackColor = true;
            this.btY.Click += new System.EventHandler(this.btY_Click);
            // 
            // btX
            // 
            this.btX.Location = new System.Drawing.Point(359, 290);
            this.btX.Name = "btX";
            this.btX.Size = new System.Drawing.Size(27, 23);
            this.btX.TabIndex = 104;
            this.btX.Tag = "X";
            this.btX.Text = "X";
            this.btX.UseVisualStyleBackColor = true;
            this.btX.Click += new System.EventHandler(this.btX_Click);
            // 
            // btW
            // 
            this.btW.Location = new System.Drawing.Point(326, 290);
            this.btW.Name = "btW";
            this.btW.Size = new System.Drawing.Size(27, 23);
            this.btW.TabIndex = 103;
            this.btW.Tag = "W";
            this.btW.Text = "W";
            this.btW.UseVisualStyleBackColor = true;
            this.btW.Click += new System.EventHandler(this.btW_Click);
            // 
            // btV
            // 
            this.btV.Location = new System.Drawing.Point(293, 290);
            this.btV.Name = "btV";
            this.btV.Size = new System.Drawing.Size(27, 23);
            this.btV.TabIndex = 102;
            this.btV.Tag = "V";
            this.btV.Text = "V";
            this.btV.UseVisualStyleBackColor = true;
            this.btV.Click += new System.EventHandler(this.btV_Click);
            // 
            // btU
            // 
            this.btU.Location = new System.Drawing.Point(260, 290);
            this.btU.Name = "btU";
            this.btU.Size = new System.Drawing.Size(27, 23);
            this.btU.TabIndex = 101;
            this.btU.Tag = "U";
            this.btU.Text = "U";
            this.btU.UseVisualStyleBackColor = true;
            this.btU.Click += new System.EventHandler(this.btU_Click);
            // 
            // btT
            // 
            this.btT.Location = new System.Drawing.Point(226, 290);
            this.btT.Name = "btT";
            this.btT.Size = new System.Drawing.Size(27, 23);
            this.btT.TabIndex = 100;
            this.btT.Tag = "T";
            this.btT.Text = "T";
            this.btT.UseVisualStyleBackColor = true;
            this.btT.Click += new System.EventHandler(this.btT_Click);
            // 
            // btS
            // 
            this.btS.Location = new System.Drawing.Point(192, 290);
            this.btS.Name = "btS";
            this.btS.Size = new System.Drawing.Size(27, 23);
            this.btS.TabIndex = 99;
            this.btS.Tag = "S";
            this.btS.Text = "S";
            this.btS.UseVisualStyleBackColor = true;
            this.btS.Click += new System.EventHandler(this.btS_Click);
            // 
            // btR
            // 
            this.btR.Location = new System.Drawing.Point(159, 290);
            this.btR.Name = "btR";
            this.btR.Size = new System.Drawing.Size(27, 23);
            this.btR.TabIndex = 98;
            this.btR.Tag = "R";
            this.btR.Text = "R";
            this.btR.UseVisualStyleBackColor = true;
            this.btR.Click += new System.EventHandler(this.btR_Click);
            // 
            // btQ
            // 
            this.btQ.Location = new System.Drawing.Point(126, 290);
            this.btQ.Name = "btQ";
            this.btQ.Size = new System.Drawing.Size(27, 23);
            this.btQ.TabIndex = 97;
            this.btQ.Tag = "Q";
            this.btQ.Text = "Q";
            this.btQ.UseVisualStyleBackColor = true;
            this.btQ.Click += new System.EventHandler(this.btQ_Click);
            // 
            // btP
            // 
            this.btP.Location = new System.Drawing.Point(93, 290);
            this.btP.Name = "btP";
            this.btP.Size = new System.Drawing.Size(27, 23);
            this.btP.TabIndex = 96;
            this.btP.Tag = "P";
            this.btP.Text = "P";
            this.btP.UseVisualStyleBackColor = true;
            this.btP.Click += new System.EventHandler(this.btP_Click);
            // 
            // btO
            // 
            this.btO.Location = new System.Drawing.Point(60, 290);
            this.btO.Name = "btO";
            this.btO.Size = new System.Drawing.Size(27, 23);
            this.btO.TabIndex = 95;
            this.btO.Tag = "O";
            this.btO.Text = "O";
            this.btO.UseVisualStyleBackColor = true;
            this.btO.Click += new System.EventHandler(this.btO_Click);
            // 
            // btN
            // 
            this.btN.Location = new System.Drawing.Point(27, 290);
            this.btN.Name = "btN";
            this.btN.Size = new System.Drawing.Size(27, 23);
            this.btN.TabIndex = 94;
            this.btN.Tag = "N";
            this.btN.Text = "N";
            this.btN.UseVisualStyleBackColor = true;
            this.btN.Click += new System.EventHandler(this.btN_Click);
            // 
            // btM
            // 
            this.btM.Location = new System.Drawing.Point(425, 261);
            this.btM.Name = "btM";
            this.btM.Size = new System.Drawing.Size(27, 23);
            this.btM.TabIndex = 93;
            this.btM.Tag = "M";
            this.btM.Text = "M";
            this.btM.UseVisualStyleBackColor = true;
            this.btM.Click += new System.EventHandler(this.btM_Click);
            // 
            // btL
            // 
            this.btL.Location = new System.Drawing.Point(392, 261);
            this.btL.Name = "btL";
            this.btL.Size = new System.Drawing.Size(27, 23);
            this.btL.TabIndex = 92;
            this.btL.Tag = "L";
            this.btL.Text = "L";
            this.btL.UseVisualStyleBackColor = true;
            this.btL.Click += new System.EventHandler(this.btL_Click);
            // 
            // btK
            // 
            this.btK.Location = new System.Drawing.Point(359, 261);
            this.btK.Name = "btK";
            this.btK.Size = new System.Drawing.Size(27, 23);
            this.btK.TabIndex = 91;
            this.btK.Tag = "K";
            this.btK.Text = "K";
            this.btK.UseVisualStyleBackColor = true;
            this.btK.Click += new System.EventHandler(this.btK_Click_1);
            // 
            // btJ
            // 
            this.btJ.Location = new System.Drawing.Point(326, 261);
            this.btJ.Name = "btJ";
            this.btJ.Size = new System.Drawing.Size(27, 23);
            this.btJ.TabIndex = 90;
            this.btJ.Tag = "J";
            this.btJ.Text = "J";
            this.btJ.UseVisualStyleBackColor = true;
            this.btJ.Click += new System.EventHandler(this.btJ_Click);
            // 
            // btI
            // 
            this.btI.Location = new System.Drawing.Point(293, 261);
            this.btI.Name = "btI";
            this.btI.Size = new System.Drawing.Size(27, 23);
            this.btI.TabIndex = 89;
            this.btI.Tag = "I";
            this.btI.Text = "I";
            this.btI.UseVisualStyleBackColor = true;
            this.btI.Click += new System.EventHandler(this.btI_Click);
            // 
            // btH
            // 
            this.btH.Location = new System.Drawing.Point(260, 261);
            this.btH.Name = "btH";
            this.btH.Size = new System.Drawing.Size(27, 23);
            this.btH.TabIndex = 88;
            this.btH.Tag = "H";
            this.btH.Text = "H";
            this.btH.UseVisualStyleBackColor = true;
            this.btH.Click += new System.EventHandler(this.btH_Click);
            // 
            // btG
            // 
            this.btG.Location = new System.Drawing.Point(225, 261);
            this.btG.Name = "btG";
            this.btG.Size = new System.Drawing.Size(27, 23);
            this.btG.TabIndex = 87;
            this.btG.Tag = "G";
            this.btG.Text = "G";
            this.btG.UseVisualStyleBackColor = true;
            this.btG.Click += new System.EventHandler(this.btG_Click);
            // 
            // btF
            // 
            this.btF.Location = new System.Drawing.Point(192, 261);
            this.btF.Name = "btF";
            this.btF.Size = new System.Drawing.Size(27, 23);
            this.btF.TabIndex = 86;
            this.btF.Tag = "F";
            this.btF.Text = "F";
            this.btF.UseVisualStyleBackColor = true;
            this.btF.Click += new System.EventHandler(this.btF_Click);
            // 
            // btE
            // 
            this.btE.Location = new System.Drawing.Point(159, 261);
            this.btE.Name = "btE";
            this.btE.Size = new System.Drawing.Size(27, 23);
            this.btE.TabIndex = 85;
            this.btE.Tag = "E";
            this.btE.Text = "E";
            this.btE.UseVisualStyleBackColor = true;
            this.btE.Click += new System.EventHandler(this.btE_Click);
            // 
            // btD
            // 
            this.btD.Location = new System.Drawing.Point(126, 261);
            this.btD.Name = "btD";
            this.btD.Size = new System.Drawing.Size(27, 23);
            this.btD.TabIndex = 84;
            this.btD.Tag = "D";
            this.btD.Text = "D";
            this.btD.UseVisualStyleBackColor = true;
            this.btD.Click += new System.EventHandler(this.btD_Click);
            // 
            // btC
            // 
            this.btC.Location = new System.Drawing.Point(93, 261);
            this.btC.Name = "btC";
            this.btC.Size = new System.Drawing.Size(27, 23);
            this.btC.TabIndex = 83;
            this.btC.Tag = "C";
            this.btC.Text = "C";
            this.btC.UseVisualStyleBackColor = true;
            this.btC.Click += new System.EventHandler(this.btC_Click);
            // 
            // btB
            // 
            this.btB.Location = new System.Drawing.Point(60, 261);
            this.btB.Name = "btB";
            this.btB.Size = new System.Drawing.Size(27, 23);
            this.btB.TabIndex = 82;
            this.btB.Tag = "B";
            this.btB.Text = "B";
            this.btB.UseVisualStyleBackColor = true;
            this.btB.Click += new System.EventHandler(this.btB_Click);
            // 
            // btA
            // 
            this.btA.Location = new System.Drawing.Point(27, 261);
            this.btA.Name = "btA";
            this.btA.Size = new System.Drawing.Size(27, 23);
            this.btA.TabIndex = 81;
            this.btA.Tag = "A";
            this.btA.Text = "A";
            this.btA.UseVisualStyleBackColor = true;
            this.btA.Click += new System.EventHandler(this.btA_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(317, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 108;
            this.label2.Text = "label2";
            // 
            // JeuPendu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 369);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btZ);
            this.Controls.Add(this.btY);
            this.Controls.Add(this.btX);
            this.Controls.Add(this.btW);
            this.Controls.Add(this.btV);
            this.Controls.Add(this.btU);
            this.Controls.Add(this.btT);
            this.Controls.Add(this.btS);
            this.Controls.Add(this.btR);
            this.Controls.Add(this.btQ);
            this.Controls.Add(this.btP);
            this.Controls.Add(this.btO);
            this.Controls.Add(this.btN);
            this.Controls.Add(this.btM);
            this.Controls.Add(this.btL);
            this.Controls.Add(this.btK);
            this.Controls.Add(this.btJ);
            this.Controls.Add(this.btI);
            this.Controls.Add(this.btH);
            this.Controls.Add(this.btG);
            this.Controls.Add(this.btF);
            this.Controls.Add(this.btE);
            this.Controls.Add(this.btD);
            this.Controls.Add(this.btC);
            this.Controls.Add(this.btB);
            this.Controls.Add(this.btA);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.pictureBox1);
            this.Name = "JeuPendu";
            this.Text = "Pendu";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button button27;
        internal System.Windows.Forms.Button btZ;
        internal System.Windows.Forms.Button btY;
        internal System.Windows.Forms.Button btX;
        internal System.Windows.Forms.Button btW;
        internal System.Windows.Forms.Button btV;
        internal System.Windows.Forms.Button btU;
        internal System.Windows.Forms.Button btT;
        internal System.Windows.Forms.Button btS;
        internal System.Windows.Forms.Button btR;
        internal System.Windows.Forms.Button btQ;
        internal System.Windows.Forms.Button btP;
        internal System.Windows.Forms.Button btO;
        internal System.Windows.Forms.Button btN;
        internal System.Windows.Forms.Button btM;
        internal System.Windows.Forms.Button btL;
        internal System.Windows.Forms.Button btK;
        internal System.Windows.Forms.Button btJ;
        internal System.Windows.Forms.Button btI;
        internal System.Windows.Forms.Button btH;
        internal System.Windows.Forms.Button btG;
        internal System.Windows.Forms.Button btF;
        internal System.Windows.Forms.Button btE;
        internal System.Windows.Forms.Button btD;
        internal System.Windows.Forms.Button btC;
        internal System.Windows.Forms.Button btB;
        internal System.Windows.Forms.Button btA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}