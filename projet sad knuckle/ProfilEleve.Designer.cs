﻿namespace projet_sad_knuckle
{
    partial class ProfilEleve
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfilEleve));
            this.pBImageProfEleve = new System.Windows.Forms.PictureBox();
            this.btPlus = new System.Windows.Forms.Button();
            this.btMoins = new System.Windows.Forms.Button();
            this.lbNote = new System.Windows.Forms.Label();
            this.lbLaNote = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.lbSaisirNom = new System.Windows.Forms.Label();
            this.btValiderNom = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btajouter = new System.Windows.Forms.Button();
            this.btHisto = new System.Windows.Forms.Button();
            this.lbNomEleve = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pBImageProfEleve)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pBImageProfEleve
            // 
            this.pBImageProfEleve.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pBImageProfEleve.Location = new System.Drawing.Point(19, 27);
            this.pBImageProfEleve.Margin = new System.Windows.Forms.Padding(4);
            this.pBImageProfEleve.Name = "pBImageProfEleve";
            this.pBImageProfEleve.Size = new System.Drawing.Size(133, 117);
            this.pBImageProfEleve.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBImageProfEleve.TabIndex = 0;
            this.pBImageProfEleve.TabStop = false;
            this.pBImageProfEleve.Visible = false;
            this.pBImageProfEleve.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btPlus
            // 
            this.btPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPlus.Image = ((System.Drawing.Image)(resources.GetObject("btPlus.Image")));
            this.btPlus.Location = new System.Drawing.Point(220, 57);
            this.btPlus.Margin = new System.Windows.Forms.Padding(4);
            this.btPlus.Name = "btPlus";
            this.btPlus.Size = new System.Drawing.Size(39, 28);
            this.btPlus.TabIndex = 1;
            this.btPlus.Tag = "1";
            this.btPlus.Text = "+";
            this.btPlus.UseVisualStyleBackColor = true;
            this.btPlus.Visible = false;
            this.btPlus.Click += new System.EventHandler(this.btPlus_Click);
            // 
            // btMoins
            // 
            this.btMoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMoins.Image = ((System.Drawing.Image)(resources.GetObject("btMoins.Image")));
            this.btMoins.Location = new System.Drawing.Point(161, 57);
            this.btMoins.Margin = new System.Windows.Forms.Padding(4);
            this.btMoins.Name = "btMoins";
            this.btMoins.Size = new System.Drawing.Size(39, 28);
            this.btMoins.TabIndex = 2;
            this.btMoins.Tag = "-1";
            this.btMoins.Text = "-";
            this.btMoins.UseVisualStyleBackColor = true;
            this.btMoins.Visible = false;
            this.btMoins.Click += new System.EventHandler(this.btMoins_Click);
            // 
            // lbNote
            // 
            this.lbNote.AutoSize = true;
            this.lbNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lbNote.Location = new System.Drawing.Point(161, 20);
            this.lbNote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNote.Name = "lbNote";
            this.lbNote.Size = new System.Drawing.Size(60, 24);
            this.lbNote.TabIndex = 5;
            this.lbNote.Text = "Note :";
            this.lbNote.Visible = false;
            // 
            // lbLaNote
            // 
            this.lbLaNote.AutoSize = true;
            this.lbLaNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lbLaNote.Location = new System.Drawing.Point(239, 20);
            this.lbLaNote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbLaNote.Name = "lbLaNote";
            this.lbLaNote.Size = new System.Drawing.Size(20, 24);
            this.lbLaNote.TabIndex = 6;
            this.lbLaNote.Text = "0";
            this.lbLaNote.Visible = false;
            this.lbLaNote.Click += new System.EventHandler(this.lbLaNote_Click);
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(143, 152);
            this.tbNom.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(120, 22);
            this.tbNom.TabIndex = 7;
            this.tbNom.Visible = false;
            // 
            // lbSaisirNom
            // 
            this.lbSaisirNom.AutoSize = true;
            this.lbSaisirNom.Location = new System.Drawing.Point(17, 155);
            this.lbSaisirNom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSaisirNom.Name = "lbSaisirNom";
            this.lbSaisirNom.Size = new System.Drawing.Size(118, 17);
            this.lbSaisirNom.TabIndex = 8;
            this.lbSaisirNom.Text = "Saisir votre nom :";
            this.lbSaisirNom.Visible = false;
            // 
            // btValiderNom
            // 
            this.btValiderNom.Location = new System.Drawing.Point(90, 178);
            this.btValiderNom.Margin = new System.Windows.Forms.Padding(4);
            this.btValiderNom.Name = "btValiderNom";
            this.btValiderNom.Size = new System.Drawing.Size(96, 27);
            this.btValiderNom.TabIndex = 9;
            this.btValiderNom.Text = "Valider nom";
            this.btValiderNom.UseVisualStyleBackColor = true;
            this.btValiderNom.Visible = false;
            this.btValiderNom.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btajouter);
            this.panel1.Controls.Add(this.btHisto);
            this.panel1.Controls.Add(this.lbNomEleve);
            this.panel1.Controls.Add(this.pBImageProfEleve);
            this.panel1.Controls.Add(this.btValiderNom);
            this.panel1.Controls.Add(this.tbNom);
            this.panel1.Controls.Add(this.btMoins);
            this.panel1.Controls.Add(this.btPlus);
            this.panel1.Controls.Add(this.lbLaNote);
            this.panel1.Controls.Add(this.lbNote);
            this.panel1.Controls.Add(this.lbSaisirNom);
            this.panel1.Location = new System.Drawing.Point(4, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 210);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btajouter
            // 
            this.btajouter.BackColor = System.Drawing.Color.White;
            this.btajouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btajouter.ForeColor = System.Drawing.Color.Black;
            this.btajouter.Location = new System.Drawing.Point(90, 67);
            this.btajouter.Name = "btajouter";
            this.btajouter.Size = new System.Drawing.Size(86, 70);
            this.btajouter.TabIndex = 12;
            this.btajouter.Text = "+";
            this.btajouter.UseVisualStyleBackColor = false;
            this.btajouter.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btHisto
            // 
            this.btHisto.Location = new System.Drawing.Point(160, 102);
            this.btHisto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btHisto.Name = "btHisto";
            this.btHisto.Size = new System.Drawing.Size(97, 35);
            this.btHisto.TabIndex = 11;
            this.btHisto.Text = "Historique";
            this.btHisto.UseVisualStyleBackColor = true;
            this.btHisto.Visible = false;
            this.btHisto.Click += new System.EventHandler(this.btHisto_Click);
            // 
            // lbNomEleve
            // 
            this.lbNomEleve.AutoSize = true;
            this.lbNomEleve.Location = new System.Drawing.Point(16, 0);
            this.lbNomEleve.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNomEleve.Name = "lbNomEleve";
            this.lbNomEleve.Size = new System.Drawing.Size(12, 17);
            this.lbNomEleve.TabIndex = 10;
            this.lbNomEleve.Text = ".";
            this.lbNomEleve.Visible = false;
            // 
            // ProfilEleve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ProfilEleve";
            this.Size = new System.Drawing.Size(309, 209);
            this.Load += new System.EventHandler(this.ProfEleve_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBImageProfEleve)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pBImageProfEleve;
        private System.Windows.Forms.Button btPlus;
        private System.Windows.Forms.Button btMoins;
        private System.Windows.Forms.Label lbNote;
        private System.Windows.Forms.Label lbLaNote;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.Label lbSaisirNom;
        private System.Windows.Forms.Button btValiderNom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbNomEleve;
        private System.Windows.Forms.Button btHisto;
        private System.Windows.Forms.Button btajouter;
    }
}
