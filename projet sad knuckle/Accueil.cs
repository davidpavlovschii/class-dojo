﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projet_sad_knuckle
{
    [Serializable]
    public partial class Accueil : Form
    {
        public Accueil()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // hide main form
            this.Hide();

            // show other form
            ClasseDojo form1 = new ClasseDojo();
            form1.ShowDialog();

            // close application
            this.Close();

        }
    }
}
